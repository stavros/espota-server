﻿espota-server
=============

espota-server is an HTTP server that can hold firmware for flashing with the
ESP8266 Arduino firmware. It's useful when you have many ESP8266-based devices
in the field and want them to download their own firmware to update themselves,
rather than you having to push firmware to each one separately.

The main difference between the espota-server and any other HTTP server is that
it only serves firmware to the device if there's a newer version of the firmware
available. If there's no newer version, it just responds with a "no new version"
and the device goes on its merry way.


Installation
------------

To install espota-server, just compile it if you have Rust installed, or get the
latest compiled binary from [the nightlies
page](https://gitlab.com/stavros/espota-server/pipelines) and drop it somewhere
on your `PATH`.


How it works
------------

It's pretty simple, just follow these steps:

1. Make sure you're [using the HTTP
OTA](https://github.com/esp8266/Arduino/blob/master/doc/ota_updates/readme.rst#advanced-updater)
library in your code, with a firmware URL like
`http://myserver:36942/projectname` (where `projectname` is your project's
name).
2. Run the espota-server somewhere.
3. Drop a file called `projectname-version.bin` in the directory where you ran
the server. Not necessarily the directory where the server *binary* is located,
just where you ran the server. For example, this file should be called
`espota-100.bin`.
4. That's it! Whenever your device attempts to update its firmware, the server
will respond with a firmware file *only* if there's a file of the same or
a later version in the directory. For example, if a device on version 8 requests
an update, and versions 7 to 22 are available on the server, the server will
respond with version 22. Of course, this means you only need to keep one file
from each project on the server at any time.


NoFUSS protocol
---------------

The server implements the NoFUSS protocol, so you can use it to update
ESPurna-based devices. To get it working, just go to your NoFUSS section
in ESPurna and enter your server's address with the /nofuss path:

http://myserver:36942/nofuss

The firmware files need to be named DEVICE_NAME-1.2.3.bin. For example, my
Sonoff Basic identifies itself as ESPURNA-SONOFF_BASI, so my firmware is
named `ESPURNA-SONOFF_BASI-1.13.2.bin`. If the firmware update fails, just
make sure you've named your files properly and put them in the directory
espota-server starts up in. That's it!

Miscellanea
-----------

Please send pull requests/comments/issues if you have any. That is all.

-- Stavros
