extern crate clap;
extern crate iron;
extern crate mount;
extern crate regex;
extern crate staticfile;
extern crate version_compare;

use clap::{value_t, App, Arg};
use iron::prelude::*;
use mount::Mount;
use regex::Regex;
use staticfile::Static;
use std::{fs, path::Path, str};
use version_compare::Version;

/**
 * Get the latest version firmware for the specific firmware name.
 *
 * Returns the version as a string along with the filename,
 * e.g. ("1.14.2-dev", "ESPURNA_S20-1.14.2-dev.bin.gz").
 */
fn get_latest_fw_version(directory: &str, fw_name: &str) -> std::io::Result<(String, String)> {
    let re = Regex::new(&format!(
        "{}{}{}",
        r"^", fw_name, r"-([\d\w\.-]+)\.bin(\.gz)?$"
    ))
    .unwrap();
    let paths: Vec<(String, String)> = fs::read_dir(directory)?
        .filter_map(|r| {
            r.ok()
                .and_then(|entry| entry.file_name().into_string().ok())
                .and_then(|filename| {
                    re.captures(&filename)
                        .map(|captures| (filename.clone(), captures.at(1).unwrap().to_owned()))
                })
        })
        .collect();

    // This hackery is because Version keeps a reference to the str used
    // to create it, so we can't return it from the loop. We can only keep
    // the filename itself and parse it again every time.
    let mut max_ver_str = "0".to_string();
    let mut max_filename = "0".to_string();
    let mut found_fw = false;

    for (filename, version) in paths {
        let ver = Version::from(&version);
        let max_ver = Version::from(&max_ver_str);
        if ver > max_ver {
            max_ver_str = version;
            max_filename = filename;
            found_fw = true;
        }
    }

    if found_fw {
        Ok((max_ver_str, max_filename))
    } else {
        Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            "No files found for this firmware.",
        ))
    }
}

/**
 * Return the version from the request headers.
 */
fn get_version_from_request(req: &mut Request) -> Result<String, &'static str> {
    let fw_version = req
        .headers
        .get_raw("x-ESP8266-version")
        .ok_or("No version header found.")?;
    str::from_utf8(&fw_version[0])
        .map(|s| s.to_string())
        .map_err(|_| "Could not parse version header.")
}

/**
 * Return the firmware name from the request headers.
 * If the relevant header is not found, return the name from the path.
 */
fn get_name_from_request(req: &mut Request) -> String {
    req.headers
        .get_raw("x-ESP8266-device")
        .map(|h| str::from_utf8(&h[0]).map(|s| s.to_string()).unwrap())
        .unwrap_or(String::from(req.url.path()[0]))
}

/**
 * The handler for a firmware request.
 */
fn firmware_request(req: &mut Request) -> IronResult<Response> {
    // Don't accept requests with no firmware name.
    let name = get_name_from_request(req);
    if name == "" {
        return Ok(Response::with((
            iron::status::BadRequest,
            "Firmware name not specified. Please visit <a href='https://gitlab.com/stavros/espota-server'>the espota-server homepage</a> to learn how to talk to this server.",
        )));
    }

    // Get the current version from the request.
    let request_version = match get_version_from_request(req) {
        Ok(ver) => ver,
        Err(e) => {
            return Ok(Response::with((iron::status::BadRequest, e)));
        }
    };
    // Enable NoFUSS-style JSON responses if necessary.
    let nofuss_protocol = req.url.path()[0] == "nofuss";
    // NoFUSS handles paths dumbly, it just concatenates what it gets here with
    // the URL. This means that we have to compensate for the lack of a slash,
    // and add a leading one here.
    let leading_slash = if req.url.path().len() == 1 { "/" } else { "" };
    let content_type = (if nofuss_protocol {
        "application/json"
    } else {
        "application/octet-stream"
    })
    .parse::<iron::mime::Mime>()
    .unwrap();

    let latest_version = get_latest_fw_version(".", &name);
    let response = match latest_version {
        Ok((ref lv, ref lf)) => {
            if Version::from(lv) > Version::from(&request_version) {
                // New version is found.

                println!("Got firmware request for \"{}\", request version was {}, newer version is available at {}.", name, request_version, lf);
                if nofuss_protocol {
                    Response::with((
                        content_type,
                        iron::status::Ok,
                        format!(
                            r#"{{"version": "{}", "firmware": "{}firmware/{}", "spiffs": ""}}"#,
                            lv, leading_slash, lf
                        ),
                    ))
                } else {
                    Response::with((iron::status::Ok, Path::new(lf)))
                }
            } else {
                // No new version is found.

                println!("Got firmware request for \"{}\", request version was {}, latest version available is {} (older).", name, request_version, lf);
                if nofuss_protocol {
                    Response::with((content_type, iron::status::Ok, "{}"))
                } else {
                    Response::with((iron::status::NotModified, "No newer firmware available."))
                }
            }
        }
        Err(_e) => {
            // No such firmware found.

            println!("Got firmware request for \"{}\", request version was {}, but there is no such firmware available.", name, request_version);
            if nofuss_protocol {
                Response::with((iron::status::Ok, "{}"))
            } else {
                Response::with((
                    content_type,
                    iron::status::NotFound,
                    if nofuss_protocol {
                        "{}"
                    } else {
                        "No firmware found by that name."
                    },
                ))
            }
        }
    };

    Ok(response)
}

fn main() {
    let matches = App::new("espota-server")
        .author("Stavros Korokithakis")
        .about("A fast and lightweight firmware update server for the ESP8266.")
        .arg(
            Arg::with_name("host")
                .short("t")
                .long("host")
                .value_name("HOST")
                .help("Sets the hostname to listen on")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .value_name("PORT")
                .help("Sets the port number to listen on")
                .takes_value(true),
        )
        .get_matches();

    let port = value_t!(matches, "port", u32).unwrap_or(36942);
    let host = matches.value_of("host").unwrap_or("127.0.0.1");

    println!("FOTA server running on http://{}:{}/", host, port);

    let mut mount = Mount::new();
    mount
        .mount("/firmware/", Static::new(Path::new(".")))
        .mount("/nofuss/firmware/", Static::new(Path::new("."))) // NoFUSS actually looks under here.
        .mount("/", firmware_request);

    Iron::new(mount)
        .http(&*format!("{}:{}", host, port.to_string()))
        .unwrap();
}
